# Paso a paso
Si estás leyendo esta guía seguramente se te ha pasado por la cabeza en algún momento lanzar tu espacio. A veces, lo único que hace falta es una chispa para dar el primer paso y que se convierta en realidad. Quizás ya has empezado a moverte para convertir ese sueño en realidad, pero te ves abrumado por la cantidad de pasos y decisiones que hay que tomar. Afortunadamente, el primer paso para lanzar un espacio es buscar a compañeros para compartir este viaje que estás a punto de comenzar.

### Reunir al grupo promotor del espacio maker

Lanzar un espacio requiere bastante esfuerzo y habilidades muy diversas. Es complicado que una sola persona pueda abarcarlo todo en solitario. Formar un equipo te ayudará a repartir el trabajo necesario, avanzar más rápido y a incluir otras perspectivas en el proyecto.

A la hora de crear este equipo hay un factor clave: busca gente con valores similares, en lugar de centrarte en las habilidades. Busca gente entusiasmada con lanzar un espacio abierto, inclusivo, diverso, útil, que invite a experimentar.

Uno de los retos de esta fase es encontrar gente que quiera crear algo parecido a lo que quieres crear tú. El formato abierto de los espacios maker hace que la gente proyecte sus ideas sobre este lienzo en blanco, sin hacerlo muchas veces de forma explícita.

En muchos casos, cuando el espacio está ya abierto, se hace evidente que cada uno de los fundadores tenía una idea totalmente diferente de lo que iba a ser el espacio. Este tipo de disputas puede hacer que el proyecto se desvanezca de un día para otro, así que es mejor tener claras las expectativas de cada uno y que se pongan por escrito.

Algunos de los ejemplos de los malentendidos que se suelen producir entre miembros fundadores de un espacio maker:

- uno de los fundadores aspira a trabajar en el espacio, sin compartir este dato con el resto de compañeros;
- cada fundador tiene en mente una forma de financiar el espacio radicalmente distinta;
- cada fundador aspira a llegar a un público concreto totalmente distinto;
- uno de los fundadores quiere montar un espacio industrial pesado, mientras que el resto aspiran a hacer un uso de herramientas de fabricación digital ligeras, etc.

Una forma de hablar este tipo de temas es pedir a cada uno de los fundadores que escriba una especie de nota de prensa, de un folio de extensión, en el que se anuncie el primer aniversario del espacio.

- ¿Qué se habrá conseguido?
- ¿Qué hechos notables han ocurrido?
- ¿Qué cosas hay en el espacio?
- ¿Quién lo usa?

Estos textos pueden servir como punto de partida para una conversación clave.

### Herramientas de comunicación

Durante el proceso de creación de nuestro espacio maker vamos a necesitar trabajar de forma constante con nuestros colaboradores. Por este motivo es importante establecer desde un primer momento cuáles van a ser la herramientas colaborativas que vamos a utilizar.

Entre las más habituales encontramos:

- Lista de correo (o Google Groups): Se trata de una lista de correo interna en la que podemos compartir información, enlaces y llevar un seguimiento de los temas que estamos tratando. Casi todos los espacios crean una lista de este tipo para que todos los usuarios del espacio compartan información. Es también muy útil para remitir avisos sobre actividades o incidencias en el espacio. Es posible que os resulte más cómodo disponer de una lista para los fundadores y otra para cuando abráis al público general, con información específica.
- Wiki: Se trata de una serie de páginas web editables por los usuarios en los que cualquiera puede editar. Permiten crear contenidos enlazados de forma sencilla, utilizando una nomenclatura específica. Puede intimidar a usuarios con conocimientos técnicos escasos.
- Dropbox: se trata un servicio en la nube que permite almacenar documentación y compartirla con otras personas. Ofrece un servicio gratuito hasta 2Gb.
- Google Drive: se trata de un servicio similar a Dropbox, ofrecido por Google.
- Google Docs: ofrece una suite ofimática online, que nos permite colaborar con otros usuarios de forma simultánea en la edición de documentos.
Slack: es una herramienta de mensajería instantánea, similar al IRC, que nos permite segmentar las conversaciones por canales. Dispone de versión web, aplicación de escritorio y móvil.
- Trello: es un gestor de proyectos ligero, que puede configurarse de múltiples modos. Los proyectos se muestran como tableros en los que podemos ir moviendo tarjetas de izquierda a derecha para actualizar su estado. Las tarjetas más a la derecha representan las tareas que ya se han completado.

Existen multitud de herramientas y alternativas para estas herramientas. Lo importante es comenzar a utilizar un serie de canales con el resto de personas que forman parte del proyecto, para acostumbrarnos a documentar y a ordenar la información que se crea en el espacio.

![](images/maker_09.jpg)

### Intereses y necesidades de los usuarios

Una vez creado este grupo promotor del espacio es hora de explorar cuáles son los intereses y las necesidades de los usuarios potenciales del espacio. Nuestra iniciativa no se lanza en el vacío, sino que por lo general se enmarcará dentro de un grupo más amplio.

Si vamos a lanzar un makespace en un colegio, será necesario aprender sobre las necesidades de los alumnos y profesores del centro. Podemos investigar dentro del currículum, pasar encuestas, realizar entrevistas, etc. Cuanto más claras tengamos las necesidades de estas personas, mejor podremos facilitar una posterior aproximación al espacio.

En muchos casos, el espacio emerge de forma natural dentro de una comunidad que ya existía. En el caso de Makespace Madrid, varios de los miembros fundadores participábamos en una serie de encuentros sobre Internet de las Cosas y los objetos conectados. Mucha gente estaba desarrollando prototipos en su casa, utilizando las placas de Arduino, y echaba en falta el acceso a herramientas de prototipado para avanzar más rápido. Pocos meses después descubrimos qué eran los Fab Labs y planteamos a algunos de los miembros más activos cómo verían el lanzamiento del espacio. Con esta información planteamos el lanzamiento de un espacio con zona de electrónica e impresión 3D.

Hay muchos espacios maker que adoptan una forma temática, trabajando la madera, cerámica, drones, textiles, experimentación científica, etc. Cuando estamos lanzando es importante abrir bien los oídos y centrar el diseño inicial en base a lo que escuchamos.

Numerosos espacios han invertido grandes cantidades de dinero en tener multitud de máquinas super avanzadas para descubrir, en su apertura, que no había ninguna persona interesada en utilizarlas. La forma de evitar que las máquinas sólo sirvan para coger polvo es diseñar a partir de las necesidades detectadas.

### 1. Adoptar una forma jurídica

Una vez tengamos claras las necesidades de nuestros potenciales usuarios, debemos pensar la forma jurídica que vamos a adoptar. Este paso es importante para disociar las acciones y responsabilidades individuales de las de las del propio espacio. Por ejemplo, cuando vayamos a alquilar un espacio o comprar una máquina, nos interesará que se haga al nombre de la entidad, en lugar de hacerlo al nombre de uno de los fundadores. La forma que se adopta de forma más convencional es la de asociación.

La asociación es una entidad sin ánimo de lucro de carácter democrático. Las asociaciones pueden registrarse a nivel nacional o autonómico, en función del ámbito de actividad. Por lo general registraremos nuestra asociación a nivel autonómico, dado que las actividades de la misma se realizarán inicialmente en nuestra ciudad.

Toda asociación debe tener unos estatutos, en los que se recoge la información clave respecto a su funcionamiento, y un acta de constitución, donde se recoge la información sobre los fundadores, sus respectivos cargos y la fecha en el que se llegó a dicho acuerdo.

En los estatutos debe especificarse el nombre de la asociación, sus objetivos y las acciones que pretende llevar a cabo para su cumplimiento. Es momento de retomar la documentación que generamos durante la creación del equipo y las necesidades que hemos descubierto en la fase anterior, para sintetizar los aspectos clave y describir cómo vamos a conseguirlos.

![](images/maker_02.jpg)

Realizar cambios en los estatutos a futuro es costoso, por lo que hay aspectos que pueden dejarse para un documento llamado Reglamento de Régimen Interno (RRI), referenciado por lo general en los estatutos. Uno de los usos habituales es decir que los miembros del espacio pagarán una cuota anual y delegar en el RRI las cuantías y los plazos. Nuestro consejo para esta fase es que toméis el modelo de estatutos de vuestra comunidad autónoma y que los personalicéis, en estos aspectos clave.

En cuanto al acta de constitución, debemos tener en cuenta la información requerida legalmente. El número mínimo de socios para crear la asociación es tres y es necesario determinar quién tendrá cada uno de los cargos dentro de la Junta Directiva. El presidente será el representante legal de la entidad; el tesorero atenderá la caja del espacio, siguiendo las órdenes del presidente; el secretario llevará todos los trámites administrativos; el vicepresidente sustituirá al presidente cuando sea necesario. Este último cargo puede solaparse con el de tesorero o secretario.

Es importante que todas las hojas se firmen por todos los socios fundadores. Haremos una copia adicional de toda la documentación firmada y la presentaremos en el registro autonómico. ¡Ya tenemos registrada nuestra asociación!

Entre los pasos administrativos que debemos dar una vez registrada la asociación está también la obtención del CIF con Hacienda, que es necesario para poder abrir una cuenta en el banco a nombre de la asociación.

Para esta fase podemos apoyarnos también en un gestor externo para realizar todos los trámites, sobre todo para que nos explique toda la operativa al detalle.

### 2. Temáticas y requisitos materiales necesarios

Una vez hemos realizado una exploración inicial de las necesidades, debemos sentarnos y seleccionar cuáles son aquellas en las que nos vamos a centrar. Nos interesa encontrar aquellos temas que tienen suficiente tirón y que pueden servirnos como catalizadores de otras actividades. Nuestra elección condicionará la elección de espacio y el equipamiento a obtener.

Hay algunas actividades que por su especial peligrosidad o impacto ambiental están reguladas. El uso del suelo en nuestras ciudades está regido por el plan urbanístico que establece cuál es el uso para cada una de las zonas y parcelas. Esto significa que no vamos a poder plantar en el centro de la ciudad una actividad que se considere nocivos. Solemos encontrar tres tipos de suelo: residencial, industrial y dotacional:

- en el residencial se permiten viviendas y casas;
- en el industrial, el uso de maquinaria pesada;
- y el dotacional está pensado para dotaciones comunes (bibliotecas, espacios deportivos, etc).

Sobre todo esto os puede asesorar el consejero urbanístico de vuestra ciudad y distrito.

Tener las temáticas claras os servirá para saber qué usos de suelo necesitáis. Si estáis pensando en montar un espacio maker repleto de fresadoras de metal, soldadores gigantes y herramientas similares, tendréis que buscar un sitio en algún polígono industrial, mientras que si estáis pensando en usos menos intensivos, podéis ampliar la búsqueda a zonas con uso residencial mixto. A nadie le gusta vivir encima de un zumbido constante, así que ¡pensad también en los vecinos!

![](images/maker_03.jpg)

El código técnico de cada ciudad es diferente para cada una de ellas, por lo que os recomendamos que consultéis con un arquitecto (o mejor todavía con un arquitecto que sepa lo que es un espacio maker) para que os oriente en la búsqueda. Podéis visitar un espacio increíble, pensar que vais a montar allí todo y descubrir a la hora de la verdad que no se puede realizar allí la actividad que queréis realizar. Una vez tengáis la información del arquitecto podéis comenzar a buscar algunos locales, utilizando algún app online, para haceros una idea de los costes.

En función de las temáticas y los requisitos, necesitamos también elaborar una lista de materiales que nos gustaría tener y hacer un cálculo general del dinero que supondría equipar nuestro espacio. En el siguiente paso daremos uso a todos estos datos.

### 3. Plan de sostenibilidad

Tal y como vimos al principio de esta guía existen dos tipos fundamentales de espacio maker: institucional y emergente. En ambos casos, deberemos diseñar un plan de sostenibilidad del proyecto y ponerlo en marcha. En el caso del espacio maker institucional, debemos presentar nuestras necesidades económicas a la institución para que sean evaluadas y aprobadas. La principal ventaja de este modelo es que delegamos en un solo agente la sostenibilidad económica del espacio. Será la institución la encargada de recaudar, por sus propios medios, los fondos necesarios para mantener el espacio abierto.

#### 3.1. Diseño del plan
Por ejemplo, en el caso de un colegio o una universidad, será la institución la que destinará fondos de las matrículas, proyectos europeos u otros tipos de fondos para este espacio maker. El principal problema es que dependeremos totalmente de este tercero para garantizar la supervivencia de nuestro espacio. Un periodo de recortes o un cambio en las políticas públicas puede dar al traste con muchísimo esfuerzo.

Los espacios emergentes suelen contar con menos presupuesto y operar en un formato más precario al principio, pero disponen de mayor libertad para explorar su modelo de sostenibilidad y crecimiento. El objetivo de estos espacios es conseguir unos ingresos mensuales que permitan sufragar los gastos derivados del alquiler, personal, seguros, luz, electricidad, etc. El modelo más habitual es el de las membresías, que funciona de forma similar al de un gimnasio.

Cada uno de los usuarios contribuye con una cantidad mensual y con estas aportaciones se van cubriendo los gastos ordinarios. Este formato básico suele complementarse con cursos de pago, tanto para miembros como para otras personas interesadas, así como el cobro por el uso de las máquinas más caras y delicadas. La máquina que suele resultar más interesante para estos menesteres es la cortadora láser, ya que permite cortar y grabar diversos materiales a toda velocidad.

Algunos espacios maker ofrecen también servicios profesionales de impresión 3D, diseño, corte laser, etc. En este caso será especialmente importante aclarar cuáles son las condiciones en las que se realizan dichos servicios para evitar malos entendidos o problemas entre miembros que realizan actividades similares dentro o fuera del propio espacio.

En cualquiera de los dos modelos, institucional o emergente, podemos tratar de incorporar sponsors o patrocinadores externos para complementar los ingresos recurrentes. Por lo general buscaremos trabajar con marcas o entidades afines los objetivos de nuestro espacio, pensando en cuáles son los beneficios que esperan a cambio de su apoyo. Puede ser mayor visibilidad para la marca, dar a conocer algún producto entre el público del espacio, apoyar nuevos creadores o proyectos concretos, etc. Trabajar con estos sponsors es una cuestión de largo plazo. ¡No pienses que por ser un espacio maker te va a llover dinero del cielo! Piensa siempre en modelos que resulten beneficiosos para ambas partes.

Una opción adicional es la aceptar donaciones en forma de dinero, equipamiento o materiales. Seguro existen otras empresas o entidades en vuestra ciudad o vuestro barrio que pueden estar interesados en impulsar el espacio de forma puntual.

Cada uno de estos canales requiere tiempo y esfuerzo, por lo que al inicio es mejor centrarse sólo en uno principal y, si acaso, uno complementario. En caso contrario corremos el riesgo de dispersar nuestras energías sin conseguir rentabilizar nuestra dedicación. Nuestro objetivo es conseguir que los ingresos recurrentes sean mayores que los gastos recurrentes mes a mes (¡para no tener que cerrar!). Una vez consigamos superar este punto de equilibrio, trataremos de ahorrar al menos tres veces nuestros gastos mensuales, para cubrir cualquier eventualidad o acontecimiento inesperado.

![](images/maker_04.jpg)

Necesitaremos realizar una provisión extra de fondos para lanzar nuestro espacio. Estos fondos serán necesarios para acondicionar el espacio, contratar los seguros iniciales y tramitar los distintos permisos. Esta cantidad oscila entre 3 y 5 veces el gasto mensual que habéis calculado.

Con tanto número, puede que tengáis la cabeza echando humo. Este puede ser un buen momento para localizar una gestoría con experiencia en asociaciones, para que nos ayude a llevar los trámites administrativos y las cuentas al día, incluyendo sus honorarios entre los gastos recurrentes.

Un aviso de advertencia respecto a los hackathones: es posible que alguien llegue a vuestro espacio para proponer un hackathon patrocinado, en el que se convoque a todos los miembros durante un fin de semana para realizar prototipos utilizando un producto concreto o dando solución a un problema específico. Los hackathones pueden ser una buena ocasión para trabajar en un proyecto concreto junto con otros miembros del espacio y explorar herramientas nuevas.

El problema con estos hackathones patrocinados es que suelen ser una forma de trabajo no remunerado. 50-60 personas compitiendo para crear un prototipo en 48 horas sin dormir, para ganar una tableta o algún otro gadget absurdo. ¿Qué ocurre al final con los proyectos creados? ¿Quién resulta el principal beneficiado? ¿Interesa más a la empresa que lo propone o a la comunidad de usuarios del espacio?

En nuestra opinión, supone un desgaste importante para el beneficio conseguido. Es más productivo quedar un día para crear proyectos para el espacio o colaborar en soluciones de código abierto. ¡Los resultados para la comunidad serán mejores!

#### 3.2. Puesta en marcha

Si hemos realizado los deberes en la anterior fase deberíamos tener claro cómo arrancar y cuáles son nuestros objetivos para conseguir un espacio maker sostenible. Nos toca alejarnos del papel y empezar a contactar con gente que pudiera estar interesada en sumarse en esta primera fase.

Una de las mejores formas para darse a conocer es comenzar a hacer actividades en distintos espacios, prestados o alquilados. De esta forma podremos ver si hay más gente con ganas de aprender y ponerse manos a la obra. Podemos organizar eventos locales temáticos relacionados con la tecnología para atraer a públicos concretos. Existe, por ejemplo, el día del hardware libre, en el que se invita a la gente realizar actividades con placas abiertos o el día de Lady Ada, para celebrar los éxitos de mujeres en ciencia y tecnología.

El boca a boca va a ser vuestra principal herramienta para convencer a otras personas en esta fase y reunir los fondos para lanzar el espacio.

Ojo, que no se trata sólo de llamar a los colegas para que se sumen al proyecto. Si el objetivo es tener un espacio para vosotros nada más, no sería necesario hacer todo esto. Uno de los alicientes fundamentales de los espacios maker es poder encontrarte con personas con experiencias y conocimientos diferentes.

### 4. Acondicionar el espacio

Al mismo tiempo que vamos consiguiendo los fondos para el proyecto, debemos comenzar la búsqueda de un espacio que se ajuste al presupuesto establecido. Intentaremos que sea un sitio accesible con transporte público, bien conectado y en buenas condiciones. Por lo general, no es interesante coger un local que esté destrozado e invertir todos nuestros esfuerzos en repararlo, puesto que será un local en alquiler y las mejoras no podremos llevarlas con nosotros. ¿Cuán lejos o cerca resulta el espacio para la comunidad dentro de la ciudad? Si todo el mundo piensa que está en el quinto pino, será mucho más complicado que podamos funcionar con un modelo de membresías y viceversa.

#### 4.1. Preparación
Respecto al espacio, invitad al arquitecto para que lo conozca antes de firmar ningún compromiso. Os podrá asesorar respecto a si el espacio puede albergar las actividades que queréis realizar. Algunas cuestiones importantes: ¿Es accesible para todos los públicos? ¿Dispone de salidas a pie de calle? ¿Se dispone de varias zonas separadas para poder disponer de una zona limpia y una zona sucia (taller)? ¿Tiene salida de humos? ¿Tiene una zona para realizar charlas/talleres/eventos?

Es importante etiquetar también las zonas potencialmente peligrosas siguiendo la normativa de riesgos laborales pertinentes. Queremos un espacio que resulte seguro para cualquier persona, pero que al mismo tiempo invite a experimentar y colaborar con otras personas.

En este momento deberíamos también contactar con varias empresas aseguradoras para pedir presupuesto para el espacio. Tenemos que contratar un seguro que cubra eventuales accidentes en el espacio, tanto personales como derivados el propio uso del espacio. Si vamos a contar con voluntarios en el espacio, podemos utilizar el seguro de la Fundación Pérez Tarrés, que nos permite suscribir un seguro para estas personas durante el desarrollo de sus funciones por tan solo 6€ por persona y año.

![](images/maker_05.jpg)

Es importante implicar al resto de la comunidad en esta fase de construcción, para que aporten sus ideas y permitan hacer un espacio maker que sientan como propio.

Una de las cosas que podemos solicitar en este momento es la conexión a Internet. Con un mobiliario mínimo y una buena conexión la gente puede empezar a utilizar el espacio para la realización de actividades ligeras. Puesto que este trámite puede demorarse en el tiempo, cuanto antes lo pidamos mejor.

#### 4.2. Equipación

Una vez tenemos organizado el espacio es hora de añadir las distintas máquinas y equipamientos básicos para empezar a trabajar. Ubicaremos las máquinas más ruidosas y sucias en una zona separada, con ventilación adecuada. Esta será la zona del taller, donde se genera polvo u otros elementos nocivos. Dispondremos también de una zona limpia, en la que colocar los ordenadores.

Esta zona limpia suele estar separada físicamente de la zona sucia por una o más puertas, para evitar que todo se llene de virutas. Nos aseguraremos en este momento también de disponer de material de seguridad como gafas, cascos antiruido, extintores. Debemos instruir a las personas que utilizan el espacio para que utilicen estos elementos al emplear las máquinas, porque en este caso es especialmente cierto el refrán: ¡más vale prevenir que curar!

La zona limpia puede servir como espacio de formación, reposicionando las mesas y haciendo un uso del proyector. Es bueno pensar cómo compatibilizar los usos para las distintas zonas.

¿Cuándo se puede hacer ruido? ¿Cuándo van a celebrarse los cursos? Veremos a continuación cómo crear este plan de actividades para comenzar a trabajar.

![](images/maker_06.jpg)

### 5. Planificar actividades

Todos los espacios maker del mundo ofrecen un día de puertas abiertas en el que dar a conocer sus actividades y conectar con potenciales miembros del espacio. Es un momento importante en el que la gente que visita el espacio toma rápidamente una decisión: ¿Me siento cómodo en este espacio? ¿Puede aportarme algo?¿Puedo aportar algo? Es importante presentar a los visitantes por sus intereses y presentarles aquellos grupos de trabajo o iniciativas que puedan estar relacionadas.

Debemos planificar también múltiples sesiones para la capacitación de los nuevos miembros del espacio. En estas sesiones explicaremos cómo utilizar las máquinas de forma segura evitando comportamientos daniños: queremos evitar que la máquina pueda dañar al usuario, al tiempo que tratamos de evitar que la persona rompa algo por hacer un mal uso de la misma.

Observaremos que de forma natural empiezan a surgir grupos de trabajo, investigación o actividades espontáneas. En estos casos, es importante fomentar que los encuentros se realicen los mismos días de forma periódica. Romper el ritmo es la forma más fácil de que la gente se desenganche.

Para facilitar estos encuentros, lo mejor es disponer de un calendario compartido en alguna herramienta como Google Calendar y permitir a todo el mundo que inscriba sus actividades, horas de apertura, etc.

Hay una actividad especial que suele realizarse también de forma periódica: la reunión de organización del mes. Dicha reunión sirve para presentar nuevas iniciativas, tomar decisiones y acordar cambios sobre las condiciones de uso del espacio. Aunque disponemos de múltiples herramientas online para colaborar, no hay nada como sentarse todo el mundo en torno a una mesa, para transmitir la información de una forma práctica.

![](images/maker_07.jpg)

Debemos tener en cuenta un último aspecto respecto a las actividades que realizamos o proponemos. Es importante que estas actividades puedan atraer distintos públicos:

- Si todas nuestras actividades son talleres de robótica y programación, llegaremos sólo a un público técnico, mayoritariamente masculino.
- Si todas nuestras actividades tienen que que ver con la electrónica, estaremos perdiendo todos los conocimientos respecto a medios de fabricación tradicionales.

Descuidar estos aspectos repercuten de forma importante en cuanto a la presencia de mujeres en estos espacios, que suele ser minoritaria en casi todo el mundo, con la excepción de México y Escocia. Para contrarrestar esta tendencia podemos invitar a mujeres que están trabajando en áreas que consideramos interesantes, para que se unan al espacio y realicen sus actividades de forma habitual.

### 6. Plan de comunicación

Una vez que tenemos planificadas nuestras primeras actividades, es hora de comunicarlas para que todo el mundo las conozca. Es el momento en el que vamos a pasar de nuestro círculo más cercano de amigos y conocidos para llegar a un público más amplio. Para ello podemos utilizar distintos medios que van desde lo físico a la digital: podemos crear folletos, panfletos o trípticos, podemos colocar posters en los locales cercanos al espacio o centrar nuestros esfuerzos en las redes sociales. Al igual que en el caso de la financiación, es importante centrarse en uno o dos medios de comunicación y explorarlos al máximo.

Cuando notemos que nuestros esfuerzos dejan de dar resultado podremos cambiar de medio. Es importante preguntar a los usuarios de nuestro espacio cuáles son las redes sociales que utilizan y trazar una estrategia al respecto.

Por ejemplo, podemos detectar que buena parte de nuestros usuarios utilizan Facebook. Si queremos entrar en contacto con más personas con intereses similares, podemos centrar nuestros esfuerzos en Facebook. Si por el contrario queremos llegar a otras personas distintas, podemos explorar otras alternativas como Snapchat, para gente más jóven, Google+ para comunidades técnicas específicas, etc.

Poco a poco irán emergiendo una serie de prácticas, valores y cultura compartida. Nuestra voz debería transmitir estos valores e inspirar a la gente para que conociera más sobre nuestro proyecto.

Esta labor de comunicación no debería estar restringida simplemente a una o dos personas, sino que es interesante que la gente se acostumbre a documentar y compartir lo que está haciendo en el espacio, hablando sobre el espacio en redes sociales. El boca a boca será durante mucho tiempo nuestra mejor herramienta para dar a conocer lo que estamos haciendo a otras personas.

Como actividad práctica para esta fase podemos reservar los nombres de dominio y usuarios de nuestro espacio en las principales redes sociales. Si estamos interesados en ofrecer cursos y actividades hacia el exterior, debemos plantearnos ¿qué plataforma utilizo para dar a conocer estas actividades? Hay multitud de plataformas como Eventbrite que nos permiten gestionar y promocionar estas reservas de forma sencilla, aunque es importante fijarse bien en los costes de las mismas.

Muchos de los espacios disponen de su propio sitio web, en el que compartir información sobre las actividades, anuncios, etc. Por lo general se utiliza un gestor de contenidos llamado WordPress para gestionar todas las publicaciones. Si no disponemos de una persona que pueda actualizar la web de forma periódica, podemos incluir la información básica estática del espacio, horarios de aperturas y enlace a nuestras redes sociales. En caso de disponer de una o más personas con tiempo suficiente, podemos incluir fotos y notas prácticas sobre los proyectos que se están llevando a cabo en el espacio.

![](images/maker_08.jpg)

#### 6.1. Celebrar la apertura
¡Llegamos al final del camino! Si hemos seguido todo el proceso, deberíamos haber podido encontrar un equipo con el que lanzar nuestro espacio, conectando con una comunidad más amplia que nosotros mismos. Hemos conseguido lanzar y abrir un espacio abierto, que invita a experimentar de una forma segura. Realizamos actividades de forma periódica y disponemos de los medios para dar a conocer lo que estamos haciendo. ¡Bravo!

Crear un espacio de este tipo es un trabajo continuo, pero antes de seguir cambiando e incluyendo cosas es importante poder celebrar con el resto del grupo todo lo que habéis conseguido.

Más allá de las propias actividades técnicas es importante dedicar un tiempo informal con el resto de usuarios del espacio. A menudo estamos tan liados con el día a día que se nos olvida parar para levantar la cabeza. Estas actividades sirven también para cimentar la comunidad. Son momentos en los que surgen temas inesperados, en los que poder preguntar a la gente si el espacio le está resultando útil, acogedor, aburrido, etc.

A nivel más profundo es un buen momento para reflexionar con el resto de personas: ¿me imaginaba así el espacio? ¿qué cosas buenas han marchado como esperábamos? ¿qué sorpresas nos hemos encontrado por el camino? ¿qué cosas no cuadran todavía? Mañana podrás ponerte en marcha de nuevo para abordar estas cuestiones, pero hoy, ha llegado el momento de descansar y celebrar junto con el resto de la comunidad de makers que ya disponéis de un espacio para crear cosas increíbles.

#### 6.2. Otros modelos de apertura y lanzamiento

En esta guía hemos hablado sobre un formato óptimo de lanzamiento, pero igual que no hay dos espacios iguales, existen otros muchos formatos alternativos que pueden funcionar en función de las circunstancias. Hackspace Londres se ha mudado ocho veces de espacio en los últimos años. Empezaron quedando en un bar, de ahí se movieron al sótano de un amigo y hoy en día utilizan una nave con 4.000 metros cuadrados.

Este movimiento no siempre ha sido por gusto o por la necesidad de crecer. Las zonas donde se asentaba el Hackspace han ido encareciéndose progresivamente, lo que ha hecho que el espacio se desplace hacia la periferia.

La Caja Makerspace (Salamanca) y Xtrene Makespace (Extremadura) lanzaron sus proyectos en un espacio cedido. Se puede comenzar con un local en una escuela o en una aceleradora de empresas de la zona. Lo importante en este caso fue disponer un espacio en el que reunirse e ir aumentando poco a poco el tamaño de sus comunidades.

El caso de Makespace Madrid ha sido interesante también. Lanzamos nuestro proyecto con una convocatoria abierta, en el que cada uno de los cinco miembros fundadores invitó a todos sus contactos, amigos, conocidos, etc. En esta convocatoria anunciamos el interés de abrir un nuevo espacio en Madrid dedicado a la fabricación digital. En ese momento no teníamos todavía local definitivo y proponíamos a la gente que se sumara a ese proceso de construcción del espacio durante seis meses. También anunciamos que las membresías del espacio serían de 30€ mensuales y pedíamos a los que quisieran ser miembros fundadores del espacio que adelantaran esos primeros seis meses.

La convocatoria fue todo un éxito con más de 125 asistentes. Una semana después habíamos conseguido... ¡¡2 apuntados!! Estaba claro que venir un día a ver de qué iba el proyecto era mucho más fácil de conseguir que hacer que la gente pusiera 180€ y se animará a participar de un espacio que no existía. Poco a poco fuimos haciendo actividades, encontramos un local, comenzamos el desembarco y al acabar los primeros seis meses éramos 70 fundadores. ¡Perseverar y conectar tuvo su recompensa!
