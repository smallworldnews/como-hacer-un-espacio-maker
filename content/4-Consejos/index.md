# Consejos

- Reutilizar elementos de personas cercanas al espacio: mucha gente dispone de herramientas y materiales perfectamente útiles que están cogiendo polvo en casa; la apertura del espacio puede utilizarse también para que, quien quiera, pueda donar estas herramientas para su uso.
- Permitir los tiempos muertos: una de las tentaciones iniciales será programar actividades en todo momento para que el sitio esté muy activo; es bueno dejar momentos en blanco para poder trabajar tranquilamente en los proyectos.
- Atender múltiples canales de comunicación: es importante facilitar la información de interés por varios sitios para asegurarnos de que llega a todas las personas del espacio: colgando carteles físicos y mensajes online, repitiendolo en múltiples ocasiones, etc.
- Evitar puntos singulares de fallo: es mejor que el peso y responsabilidad de una determinada acción se reparta entre dos o más personas; si sólo hay una y enferma o no puede afrontarla, el proyecto puede colapsar.
- Llevar la cultura del prototipado al día a día: las reglas y modos de funcionamiento del espacio no están escritos en piedra; encontrad y promoved mecanismos para aprender sobre lo que sucede en el espacio y así proponer mejores soluciones.
- Celebrar (y comunicar) los éxitos: los makers se divierten mucho creando sus proyectos, pero a veces se olvidan de parar para compartir sus invenciones con otras personas; reservad momentos para compartir estas alegrías entre los integrantes del espacio.
- Modelo flexible: este modelo puede adaptarse a colegios y bibliotecas, con equipamiento móvil, etc.; lo más importante es la metodología y fomentar la cultura de experimentación
