# Materiales

### Ordenadores
- Nos servirán para realizar nuestros diseños tanto en 2D como en 3D.
- Nos permitirán compartir estos diseños o acceder a soluciones ya creadas en Internet.

### Máquinas de fabricación digital
- Nos permitirán convertir los diseños realizados por ordenador en piezas físicas.
- Las más baratas trabajan fundiendo filamento plástico, que se va depositando capa sobre capa.
- Existen otras máquinas más rápidas para el corte de piezas planas, como las cortadoras láser o las fresadoras.

### Kits de piezas, tipo lego
- Este tipo de soluciones nos permiten generar distintas formas rápidamente y son muy útiles para generar prototipos antes de ponernos a diseñar con el ordenador.

### Kit de electrónica
- Puede incluir placas de prototipado como el microcontrolador Arduino u otras soluciones similares.

### Zonas para escribir y pensar en conjunto
- Debemos disponer de un espacio para trabajar de forma cómoda y segura.
- Es bueno disponer también de pizarras o zonas donde tomar notas, compartir nuestras ideas y bocetar soluciones de forma rápida.

### Material de papelería
- Bolígrafos, rotuladores para pizarras blancas, libretas para tomar notas sobre los proyectos, etc.

### Elementos de ordenación y almacenamiento
- Para guardar nuestros proyectos cuando no estamos trabajando en ellos y mantener el espacio limpio.
