# Introducción

Los espacios maker han proliferado por el mundo durante los últimos años, duplicando su número cada 18 meses apróximadamente. Estos espacios compartidos ofrecen el acceso a tecnologías de fabricación digital para que sus usuarios puedan materializar sus proyectos. En esta guía exploraremos qué son estos espacios, qué tipo de materiales son necesarios y cuáles son los pasos para lanzar uno. ¿Quieres crear un lugar creativo en tu comunidad donde explorar la tecnología? ¡Acompáñanos en este viaje hacia el mundo maker!

### Movimiento Maker

Desde hace unos años se habla de la palabra maker. Se trata de una palabra inglesa que significa hacedor, aunque con múltiples connotaciones. Los makers serían como los típicos manitas, pero con un gran interés por la tecnología. Una diferencia clave respecto al aficionado al bricolaje tradicional, es su interés por compartir sus creaciones con otras personas.

Muchos de los proyectos maker más conocidos, como las impresoras 3D RepRap con licencias abiertas, se basan en la colaboración de cientos de personas a través de Internet. La curiosidad lleva a estas personas a explorar técnicas distintas, mezclando varias disciplinas y creando grupos de interés de carácter esporádico.

Las herramientas de fabricación digital permiten a los makers transformar diseños realizados por ordenador en piezas físicas. En ocasiones, estas herramientas tienen un elevado coste, pero pueden utilizarse de forma compartida en espacios maker. Estos espacios sirven de punto de encuentro, lugar de fabricación y transmisión de conocimiento. Sirven para bajar las puertas de acceso a la tecnología, democratizando el acceso a la invención y creación de prototipos.

El gran número de espacios existentes por todo el mundo hace que hoy en día hablamos de un movimiento maker, que aspira a inventar, a colaborar, a aprender haciendo y a compartir con otros sus proyectos de forma abierta.

### Tipos de espacios maker
Cada espacio maker es diferente, dependiente del entorno en el que ha surgido: algunos se especializan en electrónica, impresión 3D, arte de gran formato, creación de prototipos funcionales, etc. La comunidad inicial será la que determine cuáles serán estas temáticas, que emergerán de forma natural en la mayoría de los casos.

Por lo general distinguiremos dos tipos de espacios maker:

- **Institucionales:** Estos espacios son promovidos por una institución que los alberga dentro de sus instalaciones. La sostenibilidad de este tipo de espacios depende de la financiación la institución promotora.
- **Emergentes (Grassroots):** Estos espacios surgen cuando se reúne un grupo de personas interesadas, que se organizan para lanzar y financiar el espacio por sus propios medios. Se trata de espacios independientes.

En esta guía, nos centraremos generalmente en los espacios maker de tipo emergente. Muchas de las sugerencias y consejos pueden aplicarse también a espacios institucionales, aunque son mucho más dependientes del contexto.

![](images/maker_01.jpg)

### Nombres para recordar
Existen muchos nombres para hablar de los diferentes espacios maker. Por lo general, se les suele denominar por sus nombres en inglés.

- **Fab Lab (Laboratorio de Fabricación o Laboratorio Fabuloso):** El primero de estos espacios se creó en el Instituto Tecnológico de Massachussets (MIT). Son lugares abiertos a la experimentación, que comparten una serie de equipamientos o capacidades y un código de funcionamiento común denominado Fab Charter.
- **Makerspaces (o makespaces):** son espacios independientes que ofrecen acceso a herramientas de fabricación digital y otro tipo de máquinas tradicionales. Sus usuarios suelen crear cosas para aprender cómo funcionan a nivel profundo.
- **Hackerspaces (o hackspaces):** son espacios independientes, con mayor énfasis en la responsabilidad personal de sus usuarios y en desmontar cosas para aprender cómo funcionan a nivel profundo.
- **TechShop:** son grandes espacios maker, impulsados por la empresa del mismo nombre. Ofrecen todo tipo de herramientas de fabricación y están orientados eminentemente a un público profesional.
En esta guía asumiremos que estamos hablando de makespaces cuando hablamos de espacios maker, a menos que se especifique lo contrario.

### Elementos clave
Cuando hablamos de los espacios maker se suele hacer mucho énfasis en las máquinas, aunque sólo representan una pequeña fracción de lo que son. En realidad, cuando hablamos de espacio maker estamos hablando de cuatro piezas interconectadas totalmente necesarias:

- **Comunidad:** los espacios sin usuarios son lugares inertes. Las comunidades son las que dan vida a estos espacios, creando y manteniendo sus proyectos, insuflando nuevas ideas y cuidando el espacio para uso y disfrute de todos. Las personas son el elemento clave de cualquier proyecto maker.
- **Entidad legal:** detrás de cada espacio suele haber una estructura legal que lo sostiene. Esta forma legal suele ser una asociación, con todas las obligaciones y derechos que tienen este tipo de entidades. Veremos por qué resulta clave para el lanzamiento del espacio.
- **Infraestructura:** el espacio y las máquinas son partes intrínsecas de estos proyectos. Estos elementos se utilizan de forma colaborativa por todos los usuarios del espacio, permitiendo el acceso a herramientas que difícilmente podrían adquirirse de forma individual.
- **Actividades:** son el motor del espacio. Facilitan la incorporación y capacitación de nuevos usuarios. Sirven también para formalizar los contenidos sobre los que se trabaja en el espacio.

En esta guía vamos a trabajar sobre estos cuatro puntos de forma activada para conseguir un espacio que responda a las necesidades de sus usuarios, que disponga de una estructura resiliente e invite a la gente a participar de forma activa.

Cuando se habla de espacios maker, todo el mundo imagina lugares plagados de máquinas y cachivaches, pero este equipamiento es sólo una parte del proyecto. El equipamiento podemos conseguirlo con una inversión de dinero moderada y cada día es más accesible.

Lo más importante es este proyecto es crear una comunidad de personas con ganas de aprender, explorar y compartir estas experiencias con otras personas. ¡Molar no puede pagarse con dinero! Vamos a explorar cuáles son los pasos para lanzar nuestro propio espacio.
