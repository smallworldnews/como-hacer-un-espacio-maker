# Recursos

- Canal de Youtube de Juan González. Sus famosos video tutoriales han inspirado a miles de makers a abordar sus primeros proyectos de una forma didáctica y amena.
- Comunidad Clone Warss. Esta comunidad de entusiastas ofrecen en abierto muchísimos consejos, modelos y comentarios sobre máquinas replicables.
- Manual de Supervivencia Maker. Guía que sintetiza todos los conocimientos necesarios para utilizar el equipamiento que se suele encontrar en un espacio maker.
- Aulab. Programa educativo realizado por Laboral, Centro de Arte y Creación Industrial (Gijón).
- La Hora Maker. Podcast con información práctica sobre distintas temáticas maker: recursos, eventos, actividades, etc.
- (Casi) Todo por hacer. Informe que ofrece una perspectiva del movimiento maker y los fablabs haciendo especial énfasis en España
- Grassroots Fab Lab Instructables (EN).
- Worlds of Making: Best Practices for Establishing a Makerspace for Your School (EN).
