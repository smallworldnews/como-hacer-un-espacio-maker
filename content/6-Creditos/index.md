#Créditos

### Autoría

Esta guía ha sido elaborada por César García Sáez (@elsatch), Ingeniero Técnico de Sistemas y licenciado en Estudios de Asia Oriental por la Universidad Oberta de Cataluña. Tiene más de 10 años de experiencia como trabajador del sector TIC y amplia experiencia como divulgador tecnológico. Trabaja como investigador y formador para la Asoc. EooDescubre. Graduado del programa de fabricación digital Fab Academy. Es co-fundador de Makespace Madrid (@MakespaceMadrid), una comunidad de aficionados a la tecnología y la creación que utilizan la fabricación digital para hacer realidad sus proyectos. Desde 2015, conduce el podcast "La Hora Maker", que informa sobre los avances de la comunidad maker española, poniendo en valor a los pioneros de este movimiento al tiempo que impulsa la creación de nuevos espacios y colectivos. Es vocal del capitulo español de Internet Society y de la Asociación Internacional de Fab Labs. Presidente de la red española de creación y fabricación digital (CREFAB), creada en 2016 para potenciar el ecosistema español de espacios maker.

### Licencia de uso

Las guías didácticas de La Aventura de Aprender están publicadas bajo la siguiente licencia de uso Creative Commons: CC-BY-SA 3.0. Reconocimiento – CompartirIgual (by-sa): que permite compartir, copiar y redistribuir el material en cualquier medio o formato, así como adaptar, remezclar, transformar y crear a partir del material, siempre que se reconozca la autoría del mismo y se utilice la misma licencia de uso.
